package cn.coders.net.action;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;


/**
 * 
 */

/**
 * 项目名称：struts2</BR> <BR>
 * 包名文件名：.TestAction </BR> <BR>
 * 类名称： </BR> <BR>
 * 类描述： TODO</BR> <BR>
 * 创建人：王隐by632656576@qq.com</BR> <BR>
 * 创建时间：2015-4-24 下午04:09:01 </BR> <BR>
 * 修改人：王隐 </BR> <BR>
 * 修改时间：2015-4-24 下午04:09:01 </BR> <BR>
 * 修改备注： </BR> <BR>
 * 
 * @version </BR>
 * 
 */
@ParentPackage("json-default")
@Namespace(value = "/test")
public class TestAction extends BaseAction {

    private String name;


    @Action(value = "listAnswer", results = { @Result(name = SUCCESS, location = "/answer.jsp") })
    public String listAnswer() {
        name = RandomStringUtils.randomAlphanumeric(25);
        return SUCCESS;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

}
