package cn.coders.net.action;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


/**
 * 
 */

/**
 * 项目名称：struts2</BR> <BR>
 * 包名文件名：.BaseAction </BR> <BR>
 * 类名称： </BR> <BR>
 * 类描述： TODO</BR> <BR>
 * 创建人：王隐by632656576@qq.com</BR> <BR>
 * 创建时间：2015-4-24 下午04:14:43 </BR> <BR>
 * 修改人：王隐 </BR> <BR>
 * 修改时间：2015-4-24 下午04:14:43 </BR> <BR>
 * 修改备注： </BR> <BR>
 * 
 * @version </BR>
 * 
 */
public class BaseAction extends ActionSupport {

    // 获取Attribute
    public Object getAttribute(String name) {
        return ServletActionContext.getRequest().getAttribute(name);
    }


    // 设置Attribute
    public void setAttribute(String name, Object value) {
        ServletActionContext.getRequest().setAttribute(name, value);
    }


    // 获取Parameter
    public String getParameter(String name) {
        return getRequest().getParameter(name);
    }


    // 获取Parameter数组
    public String[] getParameterValues(String name) {
        return getRequest().getParameterValues(name);
    }


    // 获取Session
    public Object getSession(String name) {
        ActionContext actionContext = ActionContext.getContext();
        Map<String, Object> session = actionContext.getSession();
        return session.get(name);
    }


    // 获取Session
    public Map<String, Object> getSession() {
        ActionContext actionContext = ActionContext.getContext();
        Map<String, Object> session = actionContext.getSession();
        return session;
    }


    // 设置Session
    public void setSession(String name, Object value) {
        ActionContext actionContext = ActionContext.getContext();
        Map<String, Object> session = actionContext.getSession();
        session.put(name, value);
    }


    // 获取Request
    public HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();
    }


    // 获取Response
    public HttpServletResponse getResponse() {
        return ServletActionContext.getResponse();
    }


    // 获取Application
    public ServletContext getApplication() {
        return ServletActionContext.getServletContext();
    }


    /**
     * 拿到session
     * 
     * @return
     */
    public HttpSession findSession() {
        return ServletActionContext.getRequest().getSession();
    }

}
